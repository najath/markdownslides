---
title: Improving Productivity Using Open Source Software
subtitle: Image Editing
author: Dr. Najath Abdul Azeez
date: \today
institute:
    - \ccbysa
logo: "images/logoFIRSTwide.png"
themeoptions:
    - progressbar=frametitle
    - subsectionpage=progressbar
---

### Outline {.c}
\tableofcontents[hideallsubsections]


# Objective

### How to create ... {.c}
![Block Diagrams](images/EV_DriveTrain.png)

### or this ...
![Certificates](images/Certificate.png){height=70%}

### or this ...
![Posters](images/Poster_Najath_ICEM.pdf){height=80%}

### or this ...
![Slides](images/SectorDetection.pdf){height=70%}



# Image Types

### Image Types
* Raster Graphics (bitmap)
    * Image is represented as a rectangular grid of pixels
* Vector Graphics
    * Image is defined in terms of mathematical formulae

### Raster Graphics (bitmap)
* Image is represented as a rectangular grid of pixels[^raster]
* main features:
    *  realistic look
    * quality loss when manipulated
        * can look blurred or pixelated
* common uses:
    * photos
    * scanned documents
    * publishing
* Common file formats:
    * JPEG, PNG, TIFF, GIF

[^raster]: [Raster graphics - Wikipedia](https://en.wikipedia.org/wiki/Raster_graphics)

### Vector Graphics
* Image is defined in terms of mathematical formulae[^vector]
    * points, lines, shapes, text, ...
* main features:
    *  high quality
    * artificial look
* Common uses:
    * clip arts, icons, logos
    * illustrations, posters
    * technical drawings
* Common file formats:
    * SVG, PDF, EPS, AI

[^vector]: [Vector graphics - Wikipedia](https://en.wikipedia.org/wiki/Vector_graphics)



# Image Editors
### Image Editors
* Vector graphics editor
    * [**Inkscape**](https://inkscape.org/)
    * [LibreOffice Draw](https://www.libreoffice.org/discover/draw/)
* Raster graphic
    * [**GIMP**](https://www.gimp.org/) - GNU Image Manipulation Program
    * [ImageMagick](https://imagemagick.org/) -  command-line interface utilities 
* Painting program
    * [Krita](https://krita.org/en/) - Digital Painting

### {.c}
\centering
**Hands-on - Image Editors**


## Inkscape - Selected Features

### Inkscape - Object Creation & Manipulation
::: nonincremental

* Object creation
    * shape tools
    * pen tools
    * text tools
    * duplicating, cloning
* Object manipulation
    * transformation
    * changing colors, gradients, patterns, strokes, markers
    * grouping, alignment, z-order, layers
    * path operations

:::

### Inkscape - Filter & Extensions
::: nonincremental

* filters
    * to add some visual effects
* extensions
    * enhanced object creation & manipulation
    * interesting extensions:
        * Generate from Path > Interpolate
        * Render > Barcode
        * Render > Function Plotter
        * Render > Mathematics > \LaTeX()

:::


## Tips

### Image Editing - tips
* Create templates and reusable assets
* Choose an appropriate color scheme
    * https://personal.sron.nl/~pault/
* Use open-source fonts
    * [Noto](https://fonts.google.com/noto)
    * [Roboto](https://fonts.google.com/specimen/Roboto)
    * [FiraGO](https://bboxtype.com/typefaces/FiraGO/#!layout=specimen)
    * [Font Awesome](https://fontawesome.com/) - for icons
    * Check [Google Fonts](https://fonts.google.com/?vfonly=true) for more


## Links

### Inkscape links
::: nonincremental

* https://inkscape-manuals.readthedocs.io/
    * https://inkscape-manuals.readthedocs.io/en/latest/creating-custom-patterns.html
* https://inkscape.org/learn/tutorials/
* https://inkscape.org/doc/keys.html
* http://www.flyertutor.com/flyer-tutorials/inkscape-tutorials.html
    * http://www.flyertutor.com/flyer-templates/inkscape-flyer-templates.html
* https://textext.github.io/textext/

:::

### GIMP links
::: nonincremental

* https://www.gimp.org/tutorials/
* https://www.gimp.org/tutorials/ImageFormats/
* https://www.gimp.org/tutorials/GIMP_Quickies/

:::



# Summary

### Summary
* There are good open-source alternatives for Image Editing
    * Avoid vendor lock-in
* Create personalized work flow
    * combination of tools
    * color schemes & fonts
    * reusable assets & templates



# 

### Image Editing - Assignment
::: nonincremental

1. Crop, resize, and remove the background from your photo using GIMP
2. Create a business card using inkscape, with your photo on it.
3. Create a poster for this program using inkscape

:::


## Questions ???


## Thank You !

###