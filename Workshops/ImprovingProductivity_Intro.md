---
title: Improving Productivity Using Open Source Software
subtitle: Introduction
author: Dr. Najath Abdul Azeez
date: \today
institute:
    - \ccbysa
logo: "images/logoFIRSTwide.png"
themeoptions:
    - progressbar=frametitle
    - subsectionpage=progressbar
---

### Outline {.c}
\tableofcontents[hideallsubsections]


# Definitions

### Productivity
**Productivity** is the efficiency of production of goods or services expressed by some measure.

. . .

*In this workshop*:
* Doing more in less time
* Taking lesser time to complete a task
* Doing a task better

### Open Source Software
Open-source software (OSS) is computer software that is released under a license in which the copyright holder grants users the rights to use, study, change, and distribute the software and its source code to anyone and for any purpose.[^OSS]

. . .

Free and open-source software (FOSS) is software that can be classified as both free software and open-source software. That is, anyone is freely licensed to use, copy, study, and change the software in any way, and the source code is openly shared so that people are encouraged to voluntarily improve the design of the software.[^FOSS]

[^OSS]: [wikipedia - "Open source software"](https://en.wikipedia.org/wiki/Open-source_software)
[^FOSS]: [wikipedia - "Free and Open source software"](https://en.wikipedia.org/wiki/Free_and_open-source_software)

### FOSS - pros and cons
. . .

:::::::::::::: {.columns}
::: {.column width="50%"}

**Pros**:
* Freedom
* Low costs
* Personal control
* Collaboration/sharing
* No vendor "lock in"

:::

. . .

::: {.column width="50%"}

**Cons**:
* Too many options/forking
* Hardware / software compatibility
* Discontinuity in development
    * Unmaintained software
* Unstable/broken software
    * at bleeding edge

:::
::::::::::::::


# Before we begin

### About this workshop series
Consists of three parts
1. Text Editing (Today, 12th Dec 2021)
2. Image Editing (Next Sunday, 19th Dec 2021)
3. Introduction to Scientific Tools (26th Dec 2021)

### Format of the workshop
* Interactive session
* Slides to give overview of key points
* Introduction to various tools / software
    * Won't go into depth
* Selected hands-on activities
* Short assignments for practicing
    * Certificate on submitting

### How to adopt OSS
* "Rome wasn't built in a day"
    * Take it slow
    * One step at a time
* Refine workflow to increase productivity
    * Keep learning
    * Continuous improvement
* Personalize the workflow and settings
    * Don't copy, get inspired!!!

### Input devices
. . .

* Keyboard - *data*
    * enter text
    * increase productivity using keyboard shortcuts
* Mouse / touch pad - *motion*
    * good for moving, drawing, pointing, ...
    * relative position
* Touch screen / pen - *position*
    * similar to Mouse / touch pad
    * absolute position
        * faster / precise

### {.c}
\centering
**Let's Begin !!!**



