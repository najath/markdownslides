---
title: Improving Productivity Using Open Source Software
subtitle: Text Editing
author: Dr. Najath Abdul Azeez
date: \today
institute:
    - \ccbysa
logo: "images/logoFIRSTwide.png"
themeoptions:
    - progressbar=frametitle
    - subsectionpage=progressbar
---

### Outline {.c}
\tableofcontents[hideallsubsections]

# Text Editors

### Text Editor
Text editor - a computer program for editing "plain" text files.

. . .

Why text editor?
* one of the most common work done on a computer
    * composing/typing new text
    * editing/rearranging existing text
    * deleting/rewriting incorrect text
* many file formats use plain text with some markup
* better text editing skills improves productivity

### Types of texts
* notes, thoughts and links
    * for personal use
* documentation and presentation
    * for sharing with others
* code, config files, logs, ...
    * for getting some "work" done

### Use cases of text editing
* cut/copy/paste
* search/replace
* work on multiple files of different types
* compare two files

### Text Editor - Desired features
* syntax highlighting
* code folding
* bracket matching
* auto indentation
* tabbed interface
* window splitting
* spell checking
* regular expression
* plugins/extensions

### Text Editor - Settings
* color scheme
* indentation/tabs
    * tab width
    * space instead of tabs
* word wrap
* backup and undo files
* line number & cursor position
* enable desired plugins

### Suggested Text Editors
* [Notepad++](https://notepad-plus-plus.org/) - *minimal*
* [Atom](https://atom.io/) - *for and by developers*
* [Kate](https://kate-editor.org/) - *lightweight with lots of features*
* [Vim](https://www.vim.org/download.php) - *powerful modal editor*
* [Emacs](https://www.gnu.org/software/emacs/) / [Spacemacs](https://www.spacemacs.org/) - *versatile text editor*

### {.c}
\centering
**Hands-on - Text Editors**

### Vim input mode
**Vim** input mode is a powerful feature available in most text editors. Learning it will make editing texts more efficient.
Suggested series for learning Vim

> * [Are Your Ready for Vim? A Beginner Guide](https://thevaluable.dev/vim-beginner/)
> * [A Vim Guide for Intermediate Users](https://thevaluable.dev/vim-intermediate/)
> * [A Vim Guide for Advanced Users](https://thevaluable.dev/vim-advanced/)
> * [A Vim Guide for Adept Users](https://thevaluable.dev/vim-adept/)
> * [A Vim Guide For Veteran Users](https://thevaluable.dev/vim-veteran/)
> * [A Vim Guide For Experts](https://thevaluable.dev/vim-expert/)



# Documentation & Presentation

### Documents
Documents are used for recording, sharing, and presenting information

. . .

A document consists of:
* content
    * text
    * equations
    * tables
    * images
* formatting / style

### Types of Document editors
* What You See Is What You Get (WYSIWYG)
    * edit both content and formatting
* What You See Is What You Mean (WYSIWYM)
    * mostly content
    * formatting specified in a "MarkUp" format
    * templates and default files used to change the "look"

### Document Editors
* [LibreOffice](https://www.libreoffice.org/)
* \LaTeX{}
    * [MiKTeX](https://miktex.org/download) - TeX distribution
    * Beamer - for creating presentation slides
    * [Overleaf](https://www.overleaf.com/) -  Online \LaTeX{} Editor
* [LyX](https://www.lyx.org/) - a GUI document processor based on \LaTeX{}

### {.c}
\centering
**Hands-on - Document Editors**

### \LaTeX{} / Beamer links
> * https://latex-tutorial.com/
> * https://en.wikibooks.org/wiki/LaTeX
> * https://www.overleaf.com/learn

> * https://latex-beamer.com/



# Note taking & mind mapping

### Note taking
Record information from different sources

. . .

Types of notes:
* linear
* non-linear
    * free form
    * tree structure

### Textual note taking
Textual note formats:
* Outliner
* Markdown
    * minimal markup of text
    * used as a quick and easy way to create formatted text
* Wiki
    * organized information on interlinked pages
* [Org mode](https://orgmode.org/)
    * note taking, planning and organizing using plain text files

### Mind mapping
Mind maps are visual organization of information

. . .

\centering
![](images/WhatIsMindMapping.jpg){width=70%}


### Note taking software
* [BasKet Note Pads](https://github.com/KDE/basket) 
    * closest to OneNote
* [Joplin](https://joplinapp.org/)
* [**QOwnNotes**](https://www.qownnotes.org/)
* [Zettlr](https://www.zettlr.com/)
* [TiddlyWiki](https://tiddlywiki.com/) - single file wiki
* [Zim](https://zim-wiki.org/) - Desktop Wiki

. . .

* [**Freeplane**](https://www.freeplane.org/wiki/index.php/Home)
    *  free mind mapping and knowledge management software
* [VYM - View Your Mind](http://www.insilmaril.de/vym/)

### {.c}
\centering
**Demo - Note taking and mind mapping**

### Pandoc - a universal document converter
[Pandoc](https://pandoc.org/) is a versatile tool that can be used to convert files from one markup format into another.

* https://pandoc.org/MANUAL.html



# Summary

### Summary
* Text editors are versatile software programs
    * Mastering one will greatly improve productivity
* Separate content from formatting while creating documents
    * Use styles and templates for beautifying the document
* Note taking software helps to be better organized
    * Notes can be a starting point for creating other documents

### Reference Links
> * [Sustainable Authorship in Plain Text using Pandoc and Markdown](https://programminghistorian.org/en/lessons/sustainable-authorship-in-plain-text-using-pandoc-and-markdown)
> * https://maehr.github.io/academic-pandoc-template/
>     * https://github.com/maehr/academic-pandoc-template
> * https://github.com/prosegrinder/pandoc-templates
> * https://github.com/Wandmalfarbe/pandoc-latex-template



# 

### Text Editing - Assignment
::: nonincremental

1. Create a markdown file capturing your notes and thoughts about this session.
    * Write the notes using QOwnNotes
2. Convert the markdown file to PDF and DOCX format.
    * Add the "*Pandoc Export*" script in QOwnNotes and use it
        * Pandoc has to be installed
        * \LaTeX{} is required for conversion to PDF
3. Create a mindmap file capturing all information in this presentation
    * Use Freeplane

:::


## Questions ???


## Thank You !

###
