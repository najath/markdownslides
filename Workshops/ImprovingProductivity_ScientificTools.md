---
title: Improving Productivity Using Open Source Software
subtitle: Introduction to Scientific Tools
author: Dr. Najath Abdul Azeez
date: \today
institute:
    - \ccbysa
logo: "images/logoFIRSTwide.png"
themeoptions:
    - progressbar=frametitle
    - subsectionpage=progressbar
---

### Outline {.c}
\tableofcontents[hideallsubsections]


# Introduction

### About
* Not comprehensive
    * there are numerous open-source scientific programs
* Focus on data visualization, extraction and analysis
    * common to all scientific disciplines
* Just an introduction

# Computational Software

### Types
* Computer Algebra Systems
    * symbolic manipulations
* Numerical or Data Analysis
    * based on numerical computation

### Computer Algebra Systems
* [Maxima](https://maxima.sourceforge.io/)
    * a system for the manipulation of symbolic and numerical expressions
* [SageMath](https://www.sagemath.org/)
    *  Mathematical software system
* [SymPy](https://www.sympy.org/en/index.html)
    * a Python library for symbolic mathematics

### Numerical Packages & Software
* [Octave](https://www.gnu.org/software/octave/)
    * MATLAB compatible software
    * can be run in GUI mode
* [R Project](https://www.r-project.org/)
    * for statistical computing
* [NumPy](https://numpy.org/) / [SciPy](https://scipy.org/)
    * python libraries for numeric/scientific computing
* [Julia Programming Language](https://julialang.org/)
    * a high-level, high-performance, dynamic programming language



# IDE & Interactive Computing

## Desktop Software

### [Scilab](https://www.scilab.org/)
* One of the two major open-source alternative to MATLAB
    * for numerical computation
* Includes Xcos
    * dynamic systems modeler and simulator
    * equivalent to Simulink

### [SageMath](https://www.sagemath.org/)
* Open-source mathematics software system
    * Computer Algebra System
* Common interface for several software packages
    *  NumPy, SciPy, matplotlib, Sympy, Maxima, GAP, FLINT, R, ...
    * python based interface
* Alternative to Magma, Maple, Mathmetica

. . .

Links:
* [Book - Sage for Undergraduates](http://gregorybard.com/sage_for_undergraduates_color.pdf.zip)
* [Book - Computational mathematics with Sagemath](http://dl.lateralis.org/public/sagebook/sagebook-ba6596d.pdf)

### [Spyder IDE](https://www.spyder-ide.org/)
* IDE for scientific programming in Python
* Features:
    * advanced editor
    * interactive console
    * variable explorer
    * data visualization
    * integrated help

### [RStudio](https://www.rstudio.com/)
* IDE for R
* Packages of interest:
    * [tidyverse](https://www.tidyverse.org/)
        * collection of R packages for data science
    * [RMarkdown](https://rmarkdown.rstudio.com/)
        * notebook interface for high quality documents & presentations
    * [knitr](https://yihui.org/knitr/)
        * Dynamic reports combining R, TeX, Markdown & HTML
    * [bookdown](https://www.bookdown.org/)
        * a knitr extension to create books


## Web-browser based

### [Project Jupyter](https://jupyter.org/)
* interactive computing for multiple programming languages
* notebook interface
    * Jupyter notebook
    * JupyterLab
* share results using **Voila**
    * hides code and data

### [The Binder Project](https://jupyter.org/binder)
* easy way of sharing computing environments
    * shareable link with custom environments
* use cases:
    * Teaching & training
    * Technical documentation
    * Educational resources
    * Reproducible data analysis
* https://mybinder.org/
    * Turn a Git repo into a collection of interactive notebooks

### {.c}
\centering
**Demo - Jupyter & Binder**



# Data Visualization & Data Extraction

### Data visualization (Plotting)
Plotting - data to graphs

. . .

* Library / package
    * [Matplotlib](https://matplotlib.org/)  -- visualization with Python
    * [ggplot2](https://ggplot2.tidyverse.org/) -- visualization package for R
* Graphical application
    * [LabPlot](https://labplot.kde.org/) -- scientific plotting and data analysis
    * [Veusz](https://veusz.github.io/) -- a scientific plotting package

### Data Extraction
Data extraction - recovering data points from graphs
* [Engauge Digitizer](https://markummitchell.github.io/engauge-digitizer/)
* LabPlot's data picker

### {.c}
\centering
**Demo - Data Extraction & Plotting**



# Summary

### Summary
* Many open-source options for scientific tools
* How to select one?
    * prefer notebook style
        * combines code with documentation
    * start with general purpose
    * switch to domain specific
        * features / packages
        * performance

### Reference Links
* [Introduction to Jupyter Notebooks](https://programminghistorian.org/en/lessons/jupyter-notebooks)



# 

### Scientific Tools - Assignment
::: nonincremental

1. Extract the data values from the four curves in the given image[^cred1]
![img](images/plotForExtraction.png){height=65%}
2. Plot the extracted values using either Veusz or LabPlot

[^cred1]: [Cree CMA1306 datasheet](https://cree-led.com/media/documents/ds-CMA1306.pdf) -- page 18

:::


## Questions ???


## Thank You !

### 
